import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FilterPipe } from './tablefilter.pipe';
import { LimitToPipe } from './tablelimitto.pipe';
import {PaginationInstance} from 'ngx-pagination';

@Component({
  selector: 'e2edt-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  @Input() rows;
  @Input() columns;
  @Input() search;
  @Input() limitTo;
  @Input() noRecordsText;
  @Input() recordsPerPage;
  paginationId;
  showPagination = false;
  isDesc = true;
  column;
  direction;
  sortIconClass ='sorting';
  sort(col){
    this.isDesc = !this.isDesc; //change the direction    
    this.column = col;
    this.direction = this.isDesc ? 1 : -1;
    this.sortIconClass = this.isDesc ? 'sorting_asc' : 'sorting_desc';
  }

  getSortIconClass(){
    return this.sortIconClass;
  }
  constructor() { 
     this.search = null;
     this.paginationId = this.generateRandomIdNumber();     
  }

  generateRandomIdNumber(){
     return Math.random();
  }

  ngOnInit() {
    
  }

  ngOnChanges(){    
      if(this.rows.length > this.recordsPerPage){      
        this.showPagination = true;
      }    
  }
}
