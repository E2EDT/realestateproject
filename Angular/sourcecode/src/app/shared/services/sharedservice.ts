import { Injectable,Component, OnInit } from '@angular/core';
import {Http, Headers} from '@angular/http'
import { Subject }    from 'rxjs/Subject';
import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { ApphttpclientService } from '../../apphttpclient.service';
import { JwtHelper} from 'angular2-jwt';

@Injectable()
export class SharedService {
    private ordersData = new BehaviorSubject(undefined);
    private ordersfetching: boolean;
    private userDate = new BehaviorSubject(undefined);
    private userfetching: boolean;
    jwtHelper: JwtHelper = new JwtHelper();
    email;
    private getUserData() {        
        return this.userDate.asObservable();
    }
    awaitUserData() {      
        if(!this.userDate.getValue() && !this.userfetching){
            this.refreshUserData();
        }
        return this.getUserData();
    }
    refreshUserData() {      
        this.userfetching = true;
        var user = this.jwtHelper.decodeToken(localStorage.getItem('retoken'))
        this._http.post("/api/user/me", {"user_name": user.sub})
        .subscribe(data => {          
            this.userfetching = false;
            this.userDate.next(data);
        },err => {
            this.userfetching = false;
            this.userDate.error(err);
        });
    }

    private getOrdersData() {        
        return this.ordersData.asObservable();
    }
    awaitOrdersData(intensional) {      
        if((!this.ordersData.getValue() && !this.ordersfetching) || intensional){
            this.refreshOrdersData();
        }
        return this.getOrdersData();
    }
    refreshOrdersData() {      
        this.ordersfetching = true;
        this._http.get('/getallorders')
        .subscribe(data => {          
            this.ordersfetching = false;
            this.ordersData.next(data);
        },err => {
            this.ordersfetching = false;
            this.ordersData.error(err);
        });
    }

    constructor(private _http: ApphttpclientService){} 

}