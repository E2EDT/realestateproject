import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ApphttpclientService } from '../../apphttpclient.service';
import { Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr';

@Injectable()

export class AuthenticationModel{

    userData;
    token;

    setUserData(response){
        this.userData = response;
    }

    getUserData(){
        return this.userData;
    }

    setToken(token){
        this.token = token;
        localStorage.setItem('retoken', token);
    }

    getToken(){
        return this.token;
    }
    
    logout(email){
        this.userData = null;
        this.token = null;
        localStorage.removeItem('retoken');
        this.apphttpclientService.post("/prelogin/logout", {"user_name": email}).subscribe(
                (res) => {
                    this.toastrService.success("You Have logged out Successfully!");
                },
                (err) => {
                    this.toastrService.error(err.json().message);
                }
            )
        this.router.navigateByUrl('prelogin')
    }

    constructor(private apphttpclientService:ApphttpclientService, private toastrService:ToastrService, private router:Router){
       
    }
    
}
