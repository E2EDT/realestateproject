import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../environments/environment';

@Injectable()
export class ApphttpclientService {

  headers = new Headers();  
  domain=environment.apiURL

  createAuthorizationHeader(headers: Headers) {
    if(localStorage.getItem('retoken'))
        headers.append("Authorization", "Bearer " +localStorage.getItem('retoken')); 
   // else
        //this.router.navigate(['/login']);
    //headers.append("Authorization", "Bearer " +"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwMDAvbG9naW4vbGRhcCIsImlhdCI6MTUwMjI4MTM1MywiZXhwIjoxNTAyMjg0OTUzLCJuYmYiOjE1MDIyODEzNTMsImp0aSI6IldUU1dIcmRCM2Q2YUw5c0giLCJzdWIiOiI1OTc5OWIyNDUwNGM5ODBmMDAwMDAzNjIiLCJhc3NvY2lhdGVJZCI6InNoaXZhcmFtLm1hbnRyaXByYWdhZGFAdGVjaHdhdmUubmV0IiwibmFtZSI6IlNoaXZhIFJhbSBHb3BhbCIsImVtcGxveWVlTnVtYmVyIjoiMTAzMjMiLCJvcmdVbml0IjpudWxsfQ.r3Yb_eT8Q0tkqkM1l7CWhjGlPw8n7K9-8EoUFlo4-7k"); 
    
  }

  get(endPoint,headers = new Headers()) {
    this.createAuthorizationHeader(headers);
    return this.http.get(this.domain+endPoint,{headers: headers});
  }

  post(endPoint, bodyParams,headers = new Headers()) { 
    this.createAuthorizationHeader(headers);   
    return this.http.post(this.domain+endPoint, bodyParams,{headers: headers});
  }

  put(endPoint, bodyParams,headers = new Headers()) {  
    this.createAuthorizationHeader(headers);  
    return this.http.put(this.domain+endPoint, bodyParams,{headers: headers});
  }
  
  constructor(private http:Http) { 
      // new RequestOptions({ headers: this.headers })
      // this.headers.append('Content-Type', 'application/json');
  }
  
}
