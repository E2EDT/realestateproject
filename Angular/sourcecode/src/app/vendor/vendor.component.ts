import { Component, OnInit } from '@angular/core';
import { AuthenticationModel } from '../shared/services/authentication.model';
import { SharedService } from '../shared/services/sharedservice';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.css']
})
export class VendorComponent implements OnInit {

  myData;
  userDataUnSub
  constructor(private authenticationModel:AuthenticationModel,private sharedService:SharedService) { 
    this.userDataUnSub = this.sharedService.awaitUserData().subscribe(user=>{
      if(user)
           this.myData = user.json();
    })
  }

  ngOnInit() {
  }
ngOnDestroy() {
  if(this.userDataUnSub)
  this.userDataUnSub.unsubscribe();
}
}
