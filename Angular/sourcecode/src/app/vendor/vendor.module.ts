import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import {SharedModule} from './../shared/shared.module'
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';

import { ToastrService } from 'ngx-toastr';
import { VendorComponent } from './vendor.component';
import { VendorRouting } from './vendor.routes';
import { OrdersOnHoldComponent } from './ordersonhold/ordersonhold.component';
import { OrderHistoryComponent } from './orderhistory/orderhistory.component';

@NgModule({
  declarations: [
        VendorComponent,
        OrderHistoryComponent,
        OrdersOnHoldComponent
  ],
  imports: [  
        VendorRouting,
        FormsModule,
        SharedModule,
        CommonModule,
        NgxMyDatePickerModule.forRoot()
  ],
  exports: [VendorComponent],
  providers: [
        ToastrService
  ]
})

export class VendorModule { }
