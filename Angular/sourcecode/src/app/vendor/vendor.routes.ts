import { Routes, RouterModule } from '@angular/router';
import { VendorComponent } from './vendor.component';
import { OrdersOnHoldComponent } from './ordersonhold/ordersonhold.component';
import { OrderHistoryComponent } from './orderhistory/orderhistory.component';

export const VendorRoutes: Routes = [
    {
        path: '',
        component: VendorComponent,
        children:[
            {
                path: '',
                redirectTo: 'ordersonhold',
                pathMatch: 'full'
            },
            {
                path: 'ordersonhold',
                component: OrdersOnHoldComponent
            },
            {
                path: 'orderhistory',
                component: OrderHistoryComponent
            }
        ]
        
    }
    
]

export const VendorRouting = RouterModule.forChild(VendorRoutes);