import { Component, OnInit } from '@angular/core';
import { ApphttpclientService } from '../../apphttpclient.service';
import { ToastrService } from 'ngx-toastr';
import { Router,ActivatedRoute } from '@angular/router';
import {Constant} from '../../constants';

@Component({
  selector: 'app-regestration',
  templateUrl: './regestration.component.html',
  styleUrls: ['./regestration.component.css']
})
export class RegestrationComponent implements OnInit {
  firmName;
  propName;
  compPanNo;
  compRegNo;
  gstNo;
  address;
  pinCode;
  landLineNum;
  phoneNum;
  faxNum;
  busiEmail;
  secEmail;
  termsAndCond;
  userType;
  itemList = [];
  selectedItems = [];
  settings = {};

  submitForm(form){   
    if(form.valid && this.termsAndCond){
      this.apphttpclientService.post(Constant.apis.newUser,
      {
          "firm_name": this.firmName,
          "user_id": {
              "user_name": this.busiEmail,
              "full_name": this.propName,
              "user_type":this.userType,
              "material" : this.selectedItems.map(i=>i.itemName)              
            },
          "proprietor_name": this.propName,
          "comp_reg_no": this.compRegNo,
          "comp_pan_no": this.compPanNo,
          "gst_no": this.gstNo,
          "address": this.address,
          "pincode": this.pinCode,
          "landline_no": this.landLineNum,
          "mobile": this.phoneNum,
          "fax": this.faxNum,
          "business_email": this.busiEmail,
          "secondary_email": this.secEmail
      }).subscribe(
        (res)=>{
          // this.toastrService.success(res.json().message);
          this.toastrService.success("Your registration has been submitted. We will notify you once it's been approved");
          this.router.navigateByUrl('/prelogin/welcome');          
        },
        (err)=>{
          this.toastrService.error(err.json().message);
        }
      )
    }
  }
  getallMaterials(){
    this.apphttpclientService.get(Constant.apis.getAllMaterials).subscribe(
      (res) => {                
        if(res.json()){
          console.log("metett",res.json())
          if(res.json()){
          this.itemList = res.json().map(m=>{return {"id":m.name,"itemName":m.name} } )

          }
        }          
      },
      (err) => {          
      }
    );
  }

  constructor(private apphttpclientService:ApphttpclientService,private activatedRoute:ActivatedRoute, private toastrService:ToastrService, private router:Router) { 
    this.activatedRoute.params.subscribe(params => {
           this.userType = params['usertype'];
           //console.log("this.routeParam",this.routeParam)
      });
      this.getallMaterials();
  }
// onItemSelect(item: any) {
//     console.log(item);
//     console.log(this.selectedItems);
//   }
  ngOnInit() {
    

    this.selectedItems = [];
    this.settings = {
      singleSelection: false,
      text: "Select Materials",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      badgeShowLimit: 3
    };
  }

}
