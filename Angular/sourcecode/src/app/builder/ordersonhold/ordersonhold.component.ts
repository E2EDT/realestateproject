import { Component, OnInit } from '@angular/core';
import { ApphttpclientService } from '../../apphttpclient.service';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationModel } from '../../shared/services/authentication.model';
import {Constant} from '../../constants';
import { SharedService } from '../../shared/services/sharedservice';

@Component({
  selector: 'app-builder-orderhistory',
  templateUrl: './ordersonhold.component.html',
  styleUrls: ['./ordersonhold.component.css']
})
export class OrdersOnHoldComponent implements OnInit {
  OrderColumns;
  rows;
  materials = [];
  invoices = [];  
  fromDate;
  toDate;
  userDataUnSub;
  myData;
  filterObject:any = {
    material:'',
    orderStatus:'',
    invoiceId:''
  };
  myOptions: INgxMyDpOptions = {
      dateFormat: 'yyyy-mm-dd',
      showTodayBtn : false
  };
  search(){
    this.filterObject = Object.assign({},this.filterObject)
    if(this.fromDate){
      this.filterObject.fromDate = this.fromDate.formatted
    }    
    if(this.toDate){
      this.filterObject.toDate = this.toDate.formatted
    }
    console.log('this.filterObject',this.filterObject)
  }

  reset(){
    this.filterObject = {
      material:'',
      orderStatus:'',
      invoiceId:'',
      firm_name:''
    };
    this.fromDate="";
    this.toDate="";
  }

  constructor(private apphttpclientService:ApphttpclientService, private sharedService:SharedService, private authenticationModel:AuthenticationModel) { 
     this.userDataUnSub = this.sharedService.awaitUserData().subscribe(user=>{
         
         if(user && !this.myData){
           this.myData = user.json();
              this.apphttpclientService.post(Constant.apis.getMyOrders,{"user_name":this.myData.user_name}).subscribe(
                res => {
                  this.rows = res.json();
                  this.rows = this.rows.filter(obj=> obj.orderStatus === Constant.statuses._2)
                  this.rows.map(obj=>{
                    if(this.materials.indexOf(obj.material)<0){
                      this.materials.push(obj.material);
                    }
                    if(this.invoices.indexOf(obj.invoiceId)<0){
                      this.invoices.push(obj.invoiceId);            
                    }
                    return '';
                  })
                },
                err => {
                  
                }
              )
         }
     })
  }

  ngOnInit() {
     this.OrderColumns = [      
      { title: "Order date", name: 'orderDate' },
      { title: "Order No", name: 'orderId' },
      { title: "Material", name: 'material' },
      { title: "UOM", name: 'uom' },
      { title: "Qty", name: 'quantity' },
      { title: "Size", name: 'size' },       
      { title: "Price", name: 'unitPrice' },
      { title: "Amount", name: 'amount' },
      { title: "Comments", name: 'comments' }
    ];
    this.rows=[];
  }
ngOnDestroy() {
  if(this.userDataUnSub)
  this.userDataUnSub.unsubscribe();
}
}
