import { Component, OnInit } from '@angular/core';
import { ApphttpclientService } from '../../apphttpclient.service';
import { ToastrService } from 'ngx-toastr';
import {SharedService} from '../../shared/services/sharedservice'
import { AuthenticationModel } from '../../shared/services/authentication.model';
import {Constant} from '../../constants';
@Component({
  selector: 'app-builder-neworders',
  templateUrl: './neworders.component.html',
  styleUrls: ['./neworders.component.css']
})
export class NewOrdersComponent implements OnInit {
  OrderColumns;
  newOrder:NewOrder;
  materials;
  uoms;
  filterObject;
  orders;
  orderForm;
  mydata;
  userDataUnSub;
  myData;
  
  submitOrder(form,resetForm){  
    if(form.valid && form.value.material!="" && form.value.uom!=""){
      form.value.user_name = this.mydata.user_name;
      this._apphttpclientService.post(Constant.apis.newOrder,form.value).subscribe(
        (res) => {              
          if(res.json()){
            this.getAllOrders(true);
            this.toastrService.success(res.json().message);
            this.newOrder=new NewOrder();
            resetForm.reset();
            this.uoms=[];
          }                
        },
        (err) => {                
              this.toastrService.error(err.json().message);
        }
      );
    }
  }
  
  getallMaterials(){
    this._apphttpclientService.get(Constant.apis.getAllMaterials).subscribe(
      (res) => {              
        if(res.json()){
          this.materials=res.json();
        }                
      },
      (err) => {

      }                
    );
  } 
  
  getuomBymaterial(materialId){
    this.newOrder.uom = "";
    this._apphttpclientService.get(Constant.apis.getuom+materialId).subscribe(
      (res) => {
        if(res.json()){
          this.uoms=res.json();
        }
      },
      (err) => {          
      }
    );
  } 
  
  getAllOrders(intens){
     this.userDataUnSub = this.sharedService.awaitUserData().subscribe(user=>{
         
         if(user && !this.myData){
           this.myData = user.json();
              this._apphttpclientService.post(Constant.apis.getMyOrders,{"user_name":this.myData.user_name}).subscribe(
                (res) => {
                  if((res && !this.orders) || intens){            
                    if(res.json()){
                        this.orders = res.json().map((d,i)=>{d['sno']=i+1; return d;});
                        this.orders = this.orders.filter(obj=> obj.orderStatus === Constant.statuses._1)
                    }
                  }
                },
                (err) => {
                }
              );
         }
     })
  }
  constructor(private _apphttpclientService:ApphttpclientService,private authenticationModel:AuthenticationModel, private sharedService:SharedService,private toastrService:ToastrService) { 
    this.getallMaterials();
    this.newOrder=new NewOrder();
    this.getAllOrders(false);
    this.mydata=authenticationModel.getUserData();
  }

  ngOnInit() {
    this.OrderColumns = [
      { title: "Item No", name: 'sno' },
      { title: "Material", name: 'material',sort:true },
      { title: "Unit of measure", name: 'uom' },
      { title: "Quantity", name: 'quantity' },
      { title: "Size", name: 'size' },
      { title: "Order Date", name: 'orderDate' },
      { title: "Order No", name: 'orderId' }
    ];
  }
  ngOnDestroy() {
  if(this.userDataUnSub)
  this.userDataUnSub.unsubscribe();
}

}

class NewOrder{
  material:string="";
  uom:string="";
  quantity:number
  size:string
  company_name:string;
}
