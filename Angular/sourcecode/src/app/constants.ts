// The file contents constant values like messages.


export const Constant = {
 
   STATUS:{
       "Pending":"Pending",
        "Onhold":"Onhold",
        "Approved":"Approved",
        "Delivered":"Delivered",
        "Rejected":"Rejected"
   },
   statuses:{
    "_1":"Pending",
    "_2":"Onhold",
    "_3":"Approved",
    "_4":"Delivered",
    "_5":"Rejected"
   },
   apis:{
       "getAllOrders":"/api/order/getallorders",
       "getVendorOrders":"/api/order/getvendororders",
       "getVendorOrderHistory":"/api/order/getvendororderhistory",
       "getAllOrdersHistory":"/api/order/getallordershistory",
       "getAllOrdersByStatusList":"/api/order/getallordersbystatus",
       "getMyOrders":"/api/order/getmyorders",
       "getMyOrdersHistory":"/api/order/getmyordershistory",
       "newOrder":"/api/order/new",
       "getAllMaterials":"/api/getallmaterials",
       "getAllVendors":"/api/user/getallapprovedvendors",
       "getuom":"/getuom/",
       "getUsersByStatus":"/api/user/getusers/",
       "orderUpdate":"/api/order/update",
       "changeStatus":"/api/admin/changestatus",
       "newUser":"/prelogin/newuser",
       "forgotPassword":"/prelogin/forgotpwd",
   }
};
