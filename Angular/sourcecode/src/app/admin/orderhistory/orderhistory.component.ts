import { Component, OnInit } from '@angular/core';
import { ApphttpclientService } from '../../apphttpclient.service';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { ToastrService } from 'ngx-toastr';
import {Constant} from '../../constants';

@Component({
  selector: 'app-admin-orderhistory',
  templateUrl: './orderhistory.component.html',
  styleUrls: ['./orderhistory.component.css']
})
export class OrderHistoryComponent implements OnInit {
  OrderColumns;
  rows;
  materials = [];
  invoices = [];
  builders = [];
  fromDate;
  toDate;
  filterObject:any = {
    material:'',
    orderStatus:'',
    invoiceId:'',
    firm_name:''
  };
  myOptions: INgxMyDpOptions = {
      dateFormat: 'yyyy-mm-dd',
      showTodayBtn : false
  };
  search(){
    this.filterObject = Object.assign({},this.filterObject)
    if(this.fromDate){
      this.filterObject.fromDate = this.fromDate.formatted
    }    
    if(this.toDate){
      this.filterObject.toDate = this.toDate.formatted
    }
    console.log('this.filterObject',this.filterObject)
  }

  reset(){
    this.filterObject = {
      material:'',
      orderStatus:'',
      invoiceId:'',
      firm_name:''
    };
    this.fromDate="";
    this.toDate="";
  }

  constructor(private apphttpclientService:ApphttpclientService) { 
    this.apphttpclientService.get(Constant.apis.getAllOrdersHistory).subscribe(
      res => {
        this.rows = res.json();
        this.rows.map(obj=>{
          if(this.materials.indexOf(obj.material)<0){
            this.materials.push(obj.material);
          }
          if(this.invoices.indexOf(obj.invoiceId)<0 && obj.invoiceId){
            this.invoices.push(obj.invoiceId);            
          }
          if(this.builders.indexOf(obj.firm_name)<0){
            this.builders.push(obj.firm_name);            
          }  
          return '';
        })
      },
      err => {
        
      }
    )
  }

  ngOnInit() {
     this.OrderColumns = [
      { title: "Builder name", name: 'firm_name',sort:true },
      { title: "Order date", name: 'orderDate' },
      { title: "Order No", name: 'orderId' },
      { title: "Material", name: 'material' },
      { title: "UOM", name: 'uom' },
      { title: "Qty", name: 'quantity' },
      { title: "Size", name: 'size' },
      { title: "Status", name: 'orderStatus' },
      { title: "Confirmation date", name: 'orderConfirmationDate' },
      { title: "Invoice no.", name: 'invoiceId' },
      { title: "Invoice date", name: 'invoiceDate' },
      { title: "Delivery qty", name: 'deliveryQuantity' },
      { title: "Unit Price", name: 'unitPrice' },
      { title: "Amount", name: 'amount' },
      { title: "Delivery date", name: 'deliveryDate' },
      { title: "Vendor no.", name: 'vendor_mobile' },
      { title: "Vendor name", name: 'vendor_firm' },
      { title: "Comments", name: 'comments' }
    ];
    this.rows=[];
  }

}
