import { Component, OnInit } from '@angular/core';
import { ApphttpclientService } from '../../apphttpclient.service';
import {SharedService} from '../../shared/services/sharedservice';
import {Constant} from '../../constants';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import { ToastrService } from 'ngx-toastr';
import {FilterPipe} from '../../shared/components/table/tablefilter.pipe'

@Component({
  selector: 'app-admin-orderdetails',
  templateUrl: './orderdetails.component.html',
  styleUrls: ['./orderdetails.component.css']
})
export class OrderDetailsComponent implements OnInit {

  OrderColumns;
  orders = []; 
  ordersCopy = []; 
  fromDate;
  toDate;
  selectedRecord:any=null;
  filterObject:any={
    material:'',
    firm_name:'',
    orderStatus:''    
  };
  rowSelected=false;
  deliveryDate;
  confirmDate;
  myOptions: INgxMyDpOptions = {
      dateFormat: 'yyyy-mm-dd',
      showTodayBtn : false
  };
  materials;
  builders = [];
  statuslist;
  filteredVendors=[];
  allVendors=[];

  updatePayload = {
    "order_id": "",
    "order_status" : 2,
    "unit_price" : null,
    "amount" : null,
    "delivery_date" : "",
    "delivery_quantity" : null,
    "confirmation_date" : "",
    "comments" : "",
    "vendor_firm":"",
    "vendor_mobile":""
  }

  onClicked(row:any){
    this.selectedRecord = row;
    this.updatePayload.order_id = row.orderId;    
    this.updatePayload.unit_price = row.unitPrice;
    this.updatePayload.amount = row.amount;
    this.updatePayload.comments = row.comments; 
    this.rowSelected = true;
    console.log("row",row)   
    this.filterVendorsBymaterial(row.material);
  }
  
  filterOrders(){  
    this.rowSelected = false;  
    this.filterObject = Object.assign({},this.filterObject);
    if(this.fromDate){
      this.filterObject.fromDate = this.fromDate.formatted
    }    
    if(this.toDate){
      this.filterObject.toDate = this.toDate.formatted
    }
    this.orders = this.filterPipe.transform(this.ordersCopy,this.filterObject)
    console.log("this.filterObject",this.filterObject)
  }
  filterVendorsBymaterial(mtrl){
    this.filteredVendors = this.allVendors.filter(v=>v.materials.indexOf(mtrl)>-1);
    console.log(this.filteredVendors)
  }
  resetOrders(){
      this.filterObject={
        material:'',
        firm_name:'',
        orderStatus:''  
      };
      this.fromDate="";
      this.toDate="";
  }
vendorSelected(event){
  console.log("event",event)
  this.updatePayload.vendor_mobile = "";
  if(typeof event == 'object'){
    this.updatePayload.vendor_mobile=event.mobile;
  }
  else
  this.updatePayload.vendor_firm="";
}
  statusChanged(){
    if(this.updatePayload.order_status !== 4){
      this.deliveryDate = "";
      this.updatePayload.delivery_quantity = null;
      this.updatePayload.delivery_date = "";
      this.confirmDate = "";
      this.updatePayload.confirmation_date = "";
    }
  }

  resetPricing(){
    this.deliveryDate = "";
    this.confirmDate = "";
    this.updatePayload = {
      "order_id": "",
      "order_status" : 2,
      "unit_price" : null,
      "amount" : null,
      "delivery_date" : "",
      "delivery_quantity" : null,
      "confirmation_date" : "",
      "comments" : "",
    "vendor_firm":"",
    "vendor_mobile":""
    }
  }

  updateAmount(){
    if(this.updatePayload.delivery_quantity){
      this.updatePayload.amount = this.updatePayload.delivery_quantity*this.updatePayload.unit_price;
    }else{
      this.updatePayload.amount = this.selectedRecord.quantity*this.updatePayload.unit_price;
    }
  }

  submitForm(form){
    if(!form.valid){
      this.toastrService.error('Please fill out all mandatory fileds.')
    }else {      
      console.log("this.updatePayload",this.updatePayload)
      if(this.updatePayload.vendor_firm && typeof this.updatePayload.vendor_firm=='object'){
        this.updatePayload.vendor_firm=this.updatePayload.vendor_firm['firm_name'];
      }
      if(this.deliveryDate){
        this.updatePayload.delivery_date = this.deliveryDate.formatted;
      }
      if(this.confirmDate){
        this.updatePayload.confirmation_date = this.confirmDate.formatted;
      }
      this.apphttpclientService.post(Constant.apis.orderUpdate,this.updatePayload).subscribe(
        res => {
          this.toastrService.success(res.json().message);
          this.resetPricing();
          this.getAllOrders(); 
        },
        err => {
          this.toastrService.error(err.json().message)
        }
      )
    }
  }

  getAllOrders(){
    this.apphttpclientService.post(Constant.apis.getAllOrdersByStatusList,{"order_status":[Constant.STATUS.Pending,Constant.STATUS.Onhold]}).subscribe(
      (res) => {
          if(res.json()){                
            this.orders = res.json().map((d,i)=>{d['sno']=i+1;  return d;});
            this.orders = this.orders.filter((order,ind) => { return (order.orderStatus == Constant.statuses._1 || order.orderStatus == Constant.statuses._2)})
            this.orders.map(s=>{
              if(this.builders.indexOf(s.firm_name)<0){
                this.builders.push(s.firm_name);
                return '';
              }
            }) 
            this.ordersCopy = JSON.parse(JSON.stringify(this.orders)); 
          }
      },
      (err) => {
          
      }
    );
    //  this.sharedService.awaitOrdersData(false).subscribe(
    //         (res) => {
    //           if(res && !this.orders){
    //           console.log("res",res.json())
    //           if(res.json()){                
    //             this.orders=res.json().map((d,i)=>{d['sno']=i+1;  return d;});
    //             this.builders = this.orders.map(s=>s.firm_name)
    //             console.log("asdsfsd",this.builders)
    //          //  this.rows = res.json().map((d,i)=>{d['sno']=i+1; d['status']=d.user_id.status; return d;});
    //          // console.log("row",this.rows)
    //           }
    //             // Navigate to set password

    //           }
    //         },
    //         (err) => {
    //             // Invalid user name
    //         }
    // );
  }

  getallMaterials(){
    this.apphttpclientService.get(Constant.apis.getAllMaterials).subscribe(
      (res) => {                
        if(res.json()){
          this.materials=res.json();    
        }          
      },
      (err) => {          
      }
    );
  } 
  getallVendors(){
      this.apphttpclientService.get(Constant.apis.getAllVendors).subscribe(
        (res) => {                
          if(res.json()){
            console.log("asdasd",res.json())   
            this.allVendors = res.json();
          }          
        },
        (err) => {          
        }
      );
    } 
  constructor(private apphttpclientService:ApphttpclientService,private filterPipe:FilterPipe, private sharedService:SharedService, private toastrService:ToastrService) { 
    this.getAllOrders();
    this.getallMaterials();
    this.getallVendors();
    this.statuslist = Object.keys(Constant.STATUS);    
  }

  ngOnInit() {
    this.OrderColumns = [
        { title: '', cellElement: 'radio', this: this, clickHandler: this.onClicked },
        { title: "Material", name: 'material' },
        { title: "Builder name", name: 'firm_name',sort:true },
        { title: "UOM", name: 'uom' },
        { title: "Qty", name: 'quantity' },
        { title: "Size", name: 'size' },
        { title: "Order date", name: 'orderDate' },
        { title: "Order No", name: 'orderId' },
        { title: "Status", name: 'orderStatus' }
      ];
    
  }

}
