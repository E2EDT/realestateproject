import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationModel } from '../shared/services/authentication.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  user_name="admin@gmail.com";
  constructor(private toastrService: ToastrService, private authenticationModel:AuthenticationModel) { }

  ngOnInit() {
  }
}
