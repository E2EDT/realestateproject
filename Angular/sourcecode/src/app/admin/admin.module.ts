import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import {SharedModule} from './../shared/shared.module'
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { ToastrService } from 'ngx-toastr';
import { AdminComponent } from './admin.component';

import { AdminRouting } from './admin.routes';
import { AdminDashboardComponent } from './dashboard/dashboard.component';
import { RegisteredUsersComponent } from './registeredusers/registeredusers.component';
import { OrderDetailsComponent } from './orderdetails/orderdetails.component';
import { OrderHistoryComponent } from './orderhistory/orderhistory.component';
import {FilterPipe} from './../shared/components/table/tablefilter.pipe'

@NgModule({
  declarations: [
        AdminComponent,
        AdminDashboardComponent,
        RegisteredUsersComponent,
        OrderDetailsComponent,
        OrderHistoryComponent
  ],
  imports: [  
        AdminRouting,
        FormsModule,
        SharedModule,
        CommonModule,
        NguiAutoCompleteModule,
        NgxMyDatePickerModule.forRoot()
  ],
  exports: [AdminComponent],
  providers: [
        ToastrService,
        FilterPipe
  ]
})

export class AdminModule { }
