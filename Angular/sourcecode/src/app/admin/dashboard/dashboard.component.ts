import { Component, OnInit } from '@angular/core';
import { ApphttpclientService } from '../../apphttpclient.service';
import {SharedService} from '../../shared/services/sharedservice';
import { AuthenticationModel } from '../../shared/services/authentication.model';
import {Constant} from '../../constants';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  userColumns;
  OrderColumns;
  orders = [];
  filterObject;
  users =[];
  vendors=[];
  builders=[];
  mydata;

  getUsers(){
    this.apphttpclientService.get(Constant.apis.getUsersByStatus+Constant.STATUS.Pending).subscribe(
      (res) => {        
        if(res.json()){
          this.users = res.json().map((d,i)=>{d['sno']=i+1; return d;});        
          console.log("users",this.users)
          this.vendors = this.users.filter(u=>u.user_type=='vendor')
          this.builders = this.users.filter(u=>u.user_type=='builder')
        }          
      },
      (err) => {          
      }
    );
  }
  
  getOrders(){
    this.apphttpclientService.get(Constant.apis.getAllOrders).subscribe(
      (res) => {
          if(res.json()){
            this.orders=res.json().map((d,i)=>{d['sno']=i+1;  return d;});          
          }
      },
      (err) => {
          
      }
    );
  }

  constructor(private apphttpclientService:ApphttpclientService, private authenticationModel:AuthenticationModel) { 
    this.getOrders();
    this.getUsers();
    this.mydata = authenticationModel.getUserData()
  }

  ngOnInit() {
    this.OrderColumns = [
      { title: "S.No", name: 'sno' },
      { title: "Material", name: 'material',sort:true },
      { title: "Unit of measure", name: 'uom' },
      { title: "Quantity", name: 'quantity' },
      { title: "Size", name: 'size' }
    ];
    this.userColumns = [
      { title: "S.No", name: 'sno' },
      { title: "First Name", name: 'firm_name',sort:true },
      { title: "Proprietor Name", name: 'proprietor_name' },
      { title: "Company Regn.No", name: 'comp_pan_no' },
    ];
  }  

}
