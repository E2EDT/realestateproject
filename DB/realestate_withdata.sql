-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.5.13 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumping data for table realestatedb1.admin: ~0 rows (approximately)
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Dumping data for table realestatedb1.builder: ~5 rows (approximately)
/*!40000 ALTER TABLE `builder` DISABLE KEYS */;
INSERT IGNORE INTO `builder` (`firm_name`, `address`, `business_email`, `comp_pan_no`, `comp_reg_no`, `fax`, `gst_no`, `landline_no`, `mobile`, `pincode`, `proprietor_name`, `secondary_email`, `user_id`) VALUES
	('MS constructions', 'HYDERANAD\n', 'MS@GMAIL.COM', 'BIGPK1764G', '47854', '9989836535', '441541', '9989836535', '9989836535', '520010', 'Prasad K', '', 4),
	('Prdeep builders', '11-33-560\nVengalrao board', 'pradeep@gmail.com', 'ASDFC1234A', NULL, '9989836535', NULL, '9989836535', '9989836535', '524201', 'Pradeep', NULL, 6),
	('RS buiilders', 'MIG 117, Bharat Nagar\nMoosapet', 'ramesh@gmail.com', 'BPSPR0880P', NULL, '9989836535', NULL, '9989836535', '9989836535', '500018', 'Ramesh', NULL, 5),
	('Surendra builders', 'MIG 117, Bharat Nagar\nMoosapet', 'surendra@gmail.com', 'ASDFC1234A', NULL, NULL, NULL, NULL, '9989836535', '500018', 'Surendra', NULL, 8),
	('Vinod Builders', '11-33-560\nVengalrao board', 'vinod@gmail.com', 'ASDFC1234A', NULL, NULL, NULL, '9989836535', '9989836535', '524201', 'Vinod', NULL, 7);
/*!40000 ALTER TABLE `builder` ENABLE KEYS */;

-- Dumping data for table realestatedb1.db_users: ~7 rows (approximately)
/*!40000 ALTER TABLE `db_users` DISABLE KEYS */;
INSERT IGNORE INTO `db_users` (`user_id`, `comments`, `doj`, `full_name`, `password`, `sqpwd`, `user_name`, `user_type`, `sqid`, `status`) VALUES
	(2, NULL, '2017-09-17', 'ADMIN', '$2a$12$XGg01FtFs/eP32HGZgBsjuMjYkv47zFj9GY0055BM2hc2CqyTERdS', '$2a$12$LhvHcc3gcxbwS7pc6ntw2OxogSxAH1TbsLCO1wRoH7pXJBgN6wqMO', 'admin@gmail.com', 'admin', 1, 3),
	(3, NULL, '2017-09-17', 'ADMIN1', NULL, NULL, 'admin1@gmail.com', 'admin', NULL, NULL),
	(4, NULL, '2017-09-17', 'Prasad K', '$2a$12$1QECwE1q2.s0WqVmAysvMOAmUg/DXFKKJ4EGaeWDfjhk7WbGgQeZu', '$2a$12$YP6q3B.e04tSf0E0Gz/hU.a78u0o3uC8mjTyDRtt9I1zI30DycR7u', 'MS@GMAIL.COM', 'builder', 2, 3),
	(5, NULL, '2017-09-17', 'Ramesh', NULL, NULL, 'ramesh@gmail.com', 'builder', NULL, 1),
	(6, NULL, '2017-09-17', 'Pradeep', NULL, NULL, 'pradeep@gmail.com', 'builder', NULL, 1),
	(7, NULL, '2017-09-17', 'Vinod', NULL, NULL, 'vinod@gmail.com', 'builder', NULL, 1),
	(8, NULL, '2017-09-17', 'Surendra', NULL, NULL, 'surendra@gmail.com', 'builder', NULL, 1);
/*!40000 ALTER TABLE `db_users` ENABLE KEYS */;

-- Dumping data for table realestatedb1.invoice: ~0 rows (approximately)
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;

-- Dumping data for table realestatedb1.material: ~3 rows (approximately)
/*!40000 ALTER TABLE `material` DISABLE KEYS */;
INSERT IGNORE INTO `material` (`id`, `name`) VALUES
	(1, 'Iron'),
	(2, 'Cement'),
	(3, 'Sand');
/*!40000 ALTER TABLE `material` ENABLE KEYS */;

-- Dumping data for table realestatedb1.orders: ~6 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT IGNORE INTO `orders` (`order_id`, `amount`, `comments`, `delivery_date`, `delivery_quantity`, `order_confirmation_date`, `order_date`, `quantity`, `size`, `unit_price`, `firm_name`, `material`, `order_status`, `uom`) VALUES
	(15, NULL, NULL, NULL, NULL, NULL, '2017-09-17', 5, '60mm', NULL, 'MS constructions', 1, 1, 1),
	(16, NULL, NULL, NULL, NULL, NULL, '2017-09-17', 23, '34 mm', NULL, 'MS constructions', 1, 1, 1),
	(17, NULL, NULL, NULL, NULL, NULL, '2017-09-17', 34, '34 mm', NULL, 'MS constructions', 2, 1, 2),
	(18, NULL, NULL, NULL, NULL, NULL, '2017-09-17', 55, '44rf', NULL, 'MS constructions', 2, 1, 2),
	(19, NULL, NULL, NULL, NULL, NULL, '2017-09-17', 345, NULL, NULL, 'MS constructions', 3, 1, 3),
	(20, NULL, NULL, NULL, NULL, NULL, '2017-09-17', 324, 'dd', NULL, 'MS constructions', 2, 1, 2);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping data for table realestatedb1.secret_questions: ~2 rows (approximately)
/*!40000 ALTER TABLE `secret_questions` DISABLE KEYS */;
INSERT IGNORE INTO `secret_questions` (`sqid`, `question`) VALUES
	(1, 'What is your petty name ?'),
	(2, 'What is your surname ?');
/*!40000 ALTER TABLE `secret_questions` ENABLE KEYS */;

-- Dumping data for table realestatedb1.statustab: ~5 rows (approximately)
/*!40000 ALTER TABLE `statustab` DISABLE KEYS */;
INSERT IGNORE INTO `statustab` (`id`, `name`) VALUES
	(1, 'Pending'),
	(2, 'Onhold'),
	(3, 'Approved'),
	(4, 'Delivered'),
	(5, 'Rejected');
/*!40000 ALTER TABLE `statustab` ENABLE KEYS */;

-- Dumping data for table realestatedb1.uom: ~3 rows (approximately)
/*!40000 ALTER TABLE `uom` DISABLE KEYS */;
INSERT IGNORE INTO `uom` (`id`, `name`, `material_id`) VALUES
	(1, 'tons', 1),
	(2, 'grade4', 2),
	(3, 'bags', 3);
/*!40000 ALTER TABLE `uom` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
